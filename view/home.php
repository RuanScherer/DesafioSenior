<?php

	session_start();

	if(!isset($_SESSION['id'])) {
		Header("Location: ../index.php");
	}

	if(@$_GET['logout'] == 1) {
		session_destroy();
		Header("Location: ../index.php");
	}

	include_once '../controller/DocumentController.php';

  $document = new DocumentController();

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.css">

    <title>Home</title>
  </head>
  <body class="bg-light">
  	<!-- HEADER -->
  	<header class="navbar d-flex justify-content-space-beetwen">
			<h2 class="my-2 font-weight-normal">Home</h2>
			<form action="?logout=1" method="post">
				<button class="btn btn-outline-danger">Sair</button>
			</form>
		</header>

  	<!-- PAGE CONTAINER -->
		<div class="container fluid d-flex flex-column align-items-center py-3">
			<h3 class="mb-3">Total de Vendas: R$<?php echo $document->getTotalSold(); ?></h3>
			<div class="d-flex justify-content-center flex-wrap" style="width:100%">
				<?php

					if($_SESSION['admin'] == 1) {
						echo "
							<!-- NEW PRODUCT -->
							<div class='card m-2 w-100' style='max-width: 400px'>
								<div class='card-body p0 d-flex flex-column'>
									<h5 class='card-title'>Novo Produto</h5>
									<p class='card-text'>Cadastre novos produtos para disponibilizá-los à venda.</p>
									<a href='new_product.php' class='text-center btn btn-outline-primary'>Ir</a>
								</div>
							</div>
						";
					}

				?>
				<!-- SEARCH PRODUCT -->
				<div class="card m-2 w-100" style="max-width: 400px">
					<div class="card-body p0 d-flex flex-column">
						<h5 class="card-title">Buscar Produtos</h5>
						<p class="card-text">Realize buscas por produtos já cadastrados na loja.</p>
						<a href="search.php" class="text-center btn btn-outline-primary">Ir</a>
					</div>
				</div>
				<!-- NEW SALE -->
				<div class="card m-2 w-100" style="max-width: 400px">
					<div class="card-body p0 d-flex flex-column">
						<h5 class="card-title">Nova Venda</h5>
						<p class="card-text">Adicione produtos e realize uma venda.</p>
						<a href="sale.php" class="text-center btn btn-outline-primary">Ir</a>
					</div>
				</div>
			</div>
		</div>

    <script src="../js/jquery.slim.min.js"></script>
    <script src="../js/popper.js"></script>
    <script src="../js/bootstrap.js"></script>
  </body>
</html>