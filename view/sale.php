<?php
  session_start();

  if(!isset($_SESSION['id'])) {
    Header("Location: ../index.php");
  }

  if(@$_GET['logout'] == 1) {
    session_destroy();
    Header("Location: ../index.php");
  }

  include_once '../controller/SaleController.php';
?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.css">

    <title>Venda</title>
  </head>
  <body class="bg-light">
    <!-- HEADER -->
    <header class="navbar d-flex justify-content-space-beetwen">
      <h2 class="my-2 font-weight-normal">Venda</h2>
      <form action="?logout=1" method="post">
        <button class="btn btn-outline-danger">Sair</button>
      </form>
    </header>

    <!-- PAGE CONTAINER -->
    <div class="container fluid d-flex flex-column align-items-center py-3">
      <div class="d-flex flex-column align-items-center w-100">
        <form class="d-flex flex-column align-items-center" action="?add=1" method="post">
          <div class="form-group">
            <label for="id">Código do Produto</label>
            <input type="text" class="form-control" id="id" name="id" required>
          </div>
          <button class="btn btn-outline-primary w-100">Adicionar</button>
          <?php
            if(@$_GET['add'] == 1) {
              $sale = new SaleController();
              $document = $sale->addProduct($_POST);
              if(!$document) {
                echo "
                  <p class='alert alert-danger alert-dismissible fade show' role='alert'>
                    Produto não cadastrado.
                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                      <span aria-hidden='true'>&times;</span>
                    </button>
                  </p>
                ";
              } else {
                setcookie("sale", $document);
                Header("Location: sale.php");
              }
            }
            if(isset($_COOKIE['sale'])) {
              echo "<span class='font-weight-bold mt-4'>Venda atual: ".$_COOKIE['sale']."</span>";
            }
          ?>
        </form>
        <h5 class="mt-4">Lista de items</h5>
        <table class='table table-bordered table-striped mt-2 w-100' style='max-width: 700px;'>
          <thead>
            <tr>
              <th scope='col'>Código</th>
              <th scope='col'>Descrição</th>
              <th scope='col'>Preço</th>
            </tr>
          </thead>
          <tbody>
          <?php
          // TEST PRODUCTS LIST EXISTENCE
          if(isset($_COOKIE['list'])){
            // RENDER LIST
            foreach ($_COOKIE['list'] as $product) {
              echo "
              <tr>
                <td>".unserialize($product)[0]."</td>
                <td>".unserialize($product)[1]."</td>
                <td>".unserialize($product)[2]."</td>
              </tr>
              ";
            }
          }
            
          ?>
          </tbody>
        </table>
        <?php

          if(@$_GET['confirm'] == 1) {
            $sale = new Sale();
            if($sale->confirm()) {
              Header("Location: sale.php");
            } else {
              echo "
                <p class='alert alert-danger alert-dismissible fade show my-2' role='alert'>
                  Erro ao confirmar venda, tente novamente.
                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                  </button>
                </p>
              ";
            }
          }

          if(@$_GET['clear'] == 1) {
            $sale = new Sale();
            if($sale->clear()) {
              Header("Location: sale.php");
            } else {
              echo "
                <p class='alert alert-danger alert-dismissible fade show my-2' role='alert'>
                  Erro ao limpar itens da venda, tente novamente.
                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                  </button>
                </p>
              ";
            }
          }

        ?>
        <div class="d-flex justify-content-center align-items-center w-100">
          <a class="btn btn-light m-2" href="home.php">Voltar</a>
          <form class="m-2" action="?clear=1" method="post">
            <button class="btn btn-outline-danger disabled" id="clear" disabled>Limpar</button>
          </form>
          <form class="m-2" action="?confirm=1" method="post">
            <button type="submit" class="btn btn-outline-success disabled" id="confirm" disabled>Confirmar</button>
          </form>
        </div>
      </div>
    </div>

    <script src="../js/jquery.slim.min.js"></script>
    <script src="../js/popper.js"></script>
    <script src="../js/bootstrap.js"></script>

    <script type="text/javascript">
      let clear = document.getElementById("clear");
      let confirm = document.getElementById("confirm");
      let td = [...document.getElementsByTagName("td")];
      if(td.length > 1) {
        confirm.classList.remove("disabled");
        confirm.disabled = false;
        clear.classList.remove("disabled");
        clear.disabled = false;
      }
    </script>
  </body>
</html>