<?php
  session_start();

  if(!isset($_SESSION['id']) || $_SESSION['admin'] != 1) {
    Header("Location: ../index.php");
  }

  if(@$_GET['logout'] == 1) {
    session_destroy();
    Header("Location: ../index.php");
  }

  include_once("../controller/ProductController.php");
?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.css">

    <title>Novo Produto</title>
  </head>
  <body class="bg-light">
    <!-- HEADER -->
    <header class="navbar d-flex justify-content-space-beetwen">
      <h2 class="my-2 font-weight-normal">Novo Produto</h2>
      <form action="?logout=1" method="post">
        <button class="btn btn-outline-danger">Sair</button>
      </form>
    </header>

    <!-- PAGE CONTAINER -->
    <div class="container fluid d-flex flex-column align-items-center py-3">
      <form style="width: 100%; max-width: 500px" action="?new=1" method="post">
        <div class="form-group">
          <label for="id">Código</label>
          <input type="text" class="form-control" id="id" name="id" required>
        </div>
        <div class="form-group">
          <label for="description">Descrição</label>
          <textarea type="text" class="form-control" id="description" name="description" rows="3" required></textarea>
        </div>
        <div class="form-group">
          <label for="price">Preço</label>
          <input type="text" class="form-control" id="price" name="price">
          <small class="form-text text-muted">Separe casas decimais utilizando "."</small>
        </div>
        <?php 

          if(@$_GET['new'] == 1) {
            $product = new ProductController($_POST);
            if(!$product->saveProduct()) {
              echo "
                <p class='alert alert-danger alert-dismissible fade show' role='alert'>
                  Erro ao cadastrar, tente novamente.
                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                  </button>
                </p>
              ";
            } else {
              echo "
                <p class='alert alert-success alert-dismissible fade show' role='alert'>
                  Produto cadastrado com sucesso.
                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                  </button>
                </p>
              ";
            }
          }

        ?>
        <a class="btn btn-light float-left" href="home.php">Voltar</a>
        <button type="submit" class="btn btn-success float-right">Cadastrar</button>
      </form>
    </div>

    <script src="../js/jquery.slim.min.js"></script>
    <script src="../js/popper.js"></script>
    <script src="../js/bootstrap.js"></script>

  </body>
</html>