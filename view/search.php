<?php

  session_start();

  if(!isset($_SESSION['id'])) {
    Header("Location: ../index.php");
  }

  if(@$_GET['logout'] == 1) {
    session_destroy();
    Header("Location: ../index.php");
  }

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.css">

    <title>Buscar Produtos</title>
  </head>
  <body class="bg-light">
    <!-- HEADER -->
    <header class="navbar d-flex justify-content-space-beetwen">
      <h2 class="my-2 font-weight-normal">Buscar Produtos</h2>
      <form action="?logout=1" method="post">
        <button class="btn btn-outline-danger">Sair</button>
      </form>
    </header>

    <!-- PAGE CONTAINER -->
    <div class="container fluid d-flex flex-column align-items-center py-3">
      <form style="max-width: 500px" class="w-100" action="?search=1" method="post">
        <div class="form-group">
          <label for="id">Código</label>
          <input type="text" class="form-control" id="id" name="id" required>
        </div>
        <a href="home.php" class="btn btn-light float-left">Voltar</a>
        <button class="btn btn-outline-primary float-right">Buscar</button>
      </form>
      <?php
        include_once('../controller/ProductController.php');

        if(@$_GET['search'] == 1) {
          $product = new ProductController($_POST);
          $res = $product->searchProduct();
          if(mysqli_num_rows($res) > 0) {
            echo "
            <table class='table table-bordered table-striped mt-4 w-100' style='max-width: 700px'>
              <thead>
                <tr>
                  <th scope='col'>Código</th>
                  <th scope='col'>Descrição</th>
                  <th scope='col'>Preço</th>
                </tr>
              </thead>
              <tbody>
            ";
            while($row = mysqli_fetch_assoc($res)) {
              echo "
              <tr>
                <td>".$row['idtb_product']."</td>
                <td>".$row['productDescription']."</td>
                <td>".$row['productPrice']."</td>
              </tr>
              ";
            }
            echo "
              </tbody>
            </table>
            ";
          } else {
            echo "
            <p class='alert alert-danger alert-dismissible fade show mt-4' role='alert'>
              Produto não cadastrado.
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>
            </p>
            ";
          }

        }
      ?>
    </div>

    <script src="../js/jquery.slim.min.js"></script>
    <script src="../js/popper.js"></script>
    <script src="../js/bootstrap.js"></script>
  </body>
</html>