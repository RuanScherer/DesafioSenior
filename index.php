<?php
	if(isset($_SESSION['id'])) {
    Header("Location: view/home.php");
  }
?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">

    <title>MyShop</title>
  </head>
  <body class="bg-light">
  	<!-- HEADER -->
  	<header class="navbar d-flex justify-content-center">
			<h2 class="my-2 font-weight-normal">Entrar</h2>
		</header>

  	<!-- PAGE CONTAINER -->
		<div class="container fluid d-flex flex-column align-items-center justify-content-center mt-4">
			<form style="width: 100%; max-width: 500px" action="?login=1" method="post">
        <div class="form-group">
          <label for="login">Login</label>
          <input type="text" class="form-control" id="login" name="login" required>
        </div>
        <div class="form-group">
          <label for="password">Senha</label>
          <input type="password" class="form-control" id="password" name="password" required>
        </div>
        <?php
					include_once("model/User.php");

					if(@$_GET['login'] == 1) {
						$user = new User($_POST);

						if($user->validate()) {
							Header("Location: view/home.php");
						} else {
							echo "
	              <p class='alert alert-danger alert-dismissible fade show' role='alert'>
	                Erro ao logar, tente novamente.
	                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
	                  <span aria-hidden='true'>&times;</span>
	                </button>
	              </p>
	            ";
						}
					}
				?>
        <button type="submit" class="btn btn-outline-primary float-right px-4">Entrar</button>
		</div>

    <script src="js/jquery.slim.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.js"></script>
  </body>
</html>