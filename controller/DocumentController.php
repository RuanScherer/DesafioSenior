	<?php

	include_once("../model/Document.php");

	class DocumentController
	{
		private $document;

		function __construct() {
			 $this->document = new Document();
		}

		function __get($prop) {
			return $this->prop;
		}

		function __set($prop, $value) {
			$this->prop = $value;
		}

		function createDocument() {
			return $this->document->create();
		}

		function getTotalSold() {
			return $this->document->getTotalSold();
		}
	}

?>