	<?php

	include_once("../model/User.php");

	class UserController
	{
		private $user;

		function __construct($post) {
			 $this->user = new User($post);
		}

		function __get($prop) {
			return $this->prop;
		}

		function __set($prop, $value) {
			$this->prop = $value;
		}

		function validate() {
			return $this->user->validate();
		}
	}

?>