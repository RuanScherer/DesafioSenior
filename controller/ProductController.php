	<?php

	include_once("../model/Product.php");

	class ProductController
	{
		private $product;

		function __construct($post) {
			 $this->product = new Product($post);
		}

		function __get($prop) {
			return $this->prop;
		}

		function __set($prop, $value) {
			$this->prop = $value;
		}

		function saveProduct() {
			return $this->product->save();
		}

		function searchProduct() {
			return $this->product->search();
		}
	}

?>