	<?php

	include_once("../model/Sale.php");

	class SaleController
	{
		private $sale;

		function __construct() {
			 $this->sale = new Sale();
		}

		function __get($prop) {
			return $this->prop;
		}

		function __set($prop, $value) {
			$this->prop = $value;
		}

		function addProduct($post) {
			return $this->sale->addProduct($post);
		}

		function confirmSale() {
			return $this->sale->confirm();
		}

		function clearSale() {
			return $this->sale->clear();
		}
	}

?>