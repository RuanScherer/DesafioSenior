<?php 

	include_once("Connection.php");

	class Document extends Connection
	{
		function create() {
			// TEST AND CREATE A NEW DOCUMENT
			$documentRes = mysqli_query($this->getConnection(), "select idtb_document from tb_document where documentConfirm = '0'");
			if(mysqli_num_rows($documentRes) < 1) {
				mysqli_query($this->getConnection(), "insert into tb_document(itemsCount, documentConfirm) values('0', '0')");
				$document = mysqli_query($this->getConnection(), "select idtb_document from tb_document where documentConfirm = '0'");
				while ($row = mysqli_fetch_assoc($document)) {
					return $row['idtb_document'];
				}
			} else {
				while ($row = mysqli_fetch_assoc($documentRes)) {
					return $row['idtb_document'];
				}
			}
		}

		function getTotalSold() {
			$confirmed = mysqli_query($this->getConnection(), "select idtb_document from tb_document where documentConfirm = '1'");
			$sum = 0;
			while($row = mysqli_fetch_assoc($confirmed)) {
				$price = mysqli_query($this->getConnection(), "select SUM(productPrice) from tb_product inner join tb_sales on idtb_product = tb_product_idtb_product where tb_document_idtb_document = '".$row['idtb_document']."'");
				while($priceRow = mysqli_fetch_assoc($price)) {
					$sum += $priceRow['SUM(productPrice)'];
				}
			}
			return $sum;
		}
	}

?>