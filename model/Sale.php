<?php

	include_once("connection.php");
	include_once("../controller/ProductController.php");
	include_once("../controller/DocumentController.php");

	class Sale extends Connection
	{
		private $document;

		function __construct() {
			$doc = new DocumentController();
			$this->document = $doc->createDocument();
		}	

		function __get($prop) {
			return $this->prop;
		}

		function __set($prop, $value) {
			$this->prop = $value;
		}

		function addProduct($post) {
			// SEARCH FOR PRODUCT
		  $product = new ProductController($post);
		  $productRes = $product->searchProduct();

		  // TEST PRODUCT EXISTENCE
		  if(mysqli_num_rows($productRes) > 0) {
        while($productRow = mysqli_fetch_assoc($productRes)) {
      		// ADD PRODUCT TO DOCUMENT
        	$res = mysqli_query($this->getConnection(), "insert into tb_sales(tb_product_idtb_product, tb_document_idtb_document) values('".$productRow['idtb_product']."', '".$this->document."')");
          // SET COOKIE WITH PRODUCT DATA
          if($res) {
          	$array = [$productRow['idtb_product'], $productRow['productDescription'], $productRow['productPrice']];
          	setcookie("list[".$productRow['idtb_product']."]", serialize($array));
          	return $this->document;
          } else {
          	return false;
          }
        }
      } else {
    		return false;
      }
		}

		function confirm() {
			$count = 0;
	  	foreach ($_COOKIE['list'] as $cookie => $value) {
				$count++;
	  		setcookie("list[".$cookie."]", "", time() - 3600);
	  	}
	  	setcookie("sale", "", time() - 3600);
	  	$res = mysqli_query($this->getConnection(), "update tb_document set documentConfirm = '1', itemsCount = '".$count."' where idtb_document = '".$this->document."'");
	  	if($res) {
	  		return true;
	  	} else {
	  		return false;
	  	}
		}

		function clear() {
			$res = mysqli_query($this->getConnection(), "delete from tb_sales where tb_document_idtb_document = '".$this->document."'");
			if($res) {
				foreach ($_COOKIE['list'] as $cookie => $value) {
          setcookie("list[".$cookie."]", "", time() - 3600);
        }
        setcookie("sale", "", time() - 3600);
				return true;
			} else {
				return false;
			}
		}
	}

?>