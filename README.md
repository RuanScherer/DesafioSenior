# Desafio Senior

## Preparação

	É essencial que você possua um servidor PHP e MySQL instalado.
	Execute o script database.sql que gerará a base de dados e cadastrará dois usuários teste.

## Usuários Teste

	- Vendedor:
		- Login: vendedor
		- Senha: 123
	- Senha:
		- Login: admin
		- Senha: 123
